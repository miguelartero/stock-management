package com.stock.dao.home;

import java.util.List;

import com.stock.model.home.Product;

public interface ProductRepository {
 
 	public void create(Product productModel);

 	public void update(Product productModel) ;
 
 	public Product getProductById(Long id);
 
 	public void delete(Long id);
 
 	public List<Product> getAllProducts();

}