package com.stock.dao.home;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.stock.model.home.Product;

@Repository
public class ProductRepositoyImp implements ProductRepository {
	
	 private static final String QUERY_GET_ALL_PRODUCT = "SELECT p FROM Product p";
	

	 @PersistenceContext
	 private EntityManager entityManager;

	 
	 public void create(Product productModel) {
		 entityManager.persist(productModel);
	 }

	 
	 public void update(Product productModel) {
		 entityManager.merge(productModel);
	 }

	 
	 public Product getProductById(Long id) {
	 return entityManager.find(Product.class, id);
	 }

	 
	 public void delete(Long id) {
		 Product productModel = getProductById(id);
		 if (productModel != null) {
			 entityManager.remove(productModel);
		 }
		 
	 }
	 
	 
	 public List<Product> getAllProducts() {		 
		 
		 Query query = entityManager.createQuery(QUERY_GET_ALL_PRODUCT);
		 return (List<Product>) query.getResultList();
	 }

}