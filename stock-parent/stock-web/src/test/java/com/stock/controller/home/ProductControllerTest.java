package com.stock.controller.home;

import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.doNothing;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.stock.model.home.Product;
import com.stock.services.home.ProductService;

@RunWith(MockitoJUnitRunner.class)
public class ProductControllerTest {

    @InjectMocks
    private ProductControllerImp productController;
    
    @Mock
    private ProductService productService;
    
    @Mock
    private List<Product> listProductMock;
    
    @Mock
    private Product productMock;
    
    private Product product;
    
    @Test 
    public void shouldCreateSomeProductWhenCreateSomeProduct() {
    	givenCreateSomeProduct();
    	whenCreateSomeProduct();
    	thenCreateSomeProduct();
    }
    
    private void givenCreateSomeProduct() {
    	doNothing().when(productService).create(anyObject());
    }
    
    private void whenCreateSomeProduct() {
    	productController.createSomeInitialProducts();
    }
    
    private void thenCreateSomeProduct() {
    	verify(productService, times(3)).create(anyObject());
    }
    
    
    
    @Test
    public void shouldReturnTheListProductWhenGetAllProducts() {
        givenGetAllProducts();
        List<Product> result = whenGetAllProducts();
        thenGetAllProducts(result);
    }
    
    private void givenGetAllProducts() {
    	when(productService.getAllProducts()).thenReturn(listProductMock);
    }
    
    private List<Product> whenGetAllProducts() {
    	return productController.getAllProducts();
    }
    
    private void thenGetAllProducts(List<Product> result) {
        verify(productService).getAllProducts();
        assertEquals(listProductMock, result);
    }

    
    
    @Test
    public void shouldReturnAProductWhenGetProductById() {
    	givenAIdProduct();
    	Product result = whenGetProductById(anyLong());
    	thenReturnAProduct(result);
    }
    
    private void givenAIdProduct() {
    	when(productService.getProductById(anyLong())).thenReturn(productMock);
    }
    
    private Product whenGetProductById(Long id) {
    	return productController.getProduct(id);
    }
    
    private void thenReturnAProduct(Product result) {
    	verify(productService).getProductById(anyLong());
    	assertEquals(productMock, result);
    }
    
    
    
    @Test
    public void shouldDecreaseTheStockWhenBuyProduct() {
    	givenAIdProductNoMock();
    	whenBuyProduct(anyLong());
    	thenReturnDecreaseStock();
    }
    
    private void givenAIdProductNoMock() {
    	product = new Product();
    	product.setStock(10);
    	when(productService.getProductById(anyLong())).thenReturn(product);
    	doNothing().when(productService).update(anyObject());
    }
    
    private void whenBuyProduct(Long id) {
    	productController.buyProduct(id);
    }
    
    private void thenReturnDecreaseStock() {
    	verify(productService).getProductById(anyLong());
    	verify(productService).update(anyObject());
    	assertEquals(9, product.getStock());
    }
    
    
    
    @Test
    public void shouldIncreaseTheStockWhenRefillProduct() {
    	givenAIdProductNoMock();
    	whenRefillProduct(anyLong());
    	thenReturnIncreaseStock();
    }
    
    private void whenRefillProduct(Long id) {
    	productController.refillProduct(id);
    }
    
    private void thenReturnIncreaseStock() {
    	verify(productService).getProductById(anyLong());
    	verify(productService).update(anyObject());
    	assertEquals(11, product.getStock());
    }
}
