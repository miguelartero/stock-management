package com.stock.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

import com.stock.model.home.Product;

@SpringBootApplication
@ComponentScan({ "com.stock" })
@EntityScan({ "com.stock.model" })
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}