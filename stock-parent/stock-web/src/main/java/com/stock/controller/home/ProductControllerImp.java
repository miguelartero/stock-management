package com.stock.controller.home;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stock.model.home.Product;
import com.stock.services.home.ProductService;

@RestController
public class ProductControllerImp implements ProductController {

    private static final Logger logger = LoggerFactory.getLogger(ProductControllerImp.class);

    @Autowired
    private ProductService productService;
    
    public void createSomeInitialProducts() {
    	
		 Product productModel = new Product();
		 
		 productModel.setImage("https://images.apple.com/v/ipad/home/ac/images/home/hero_alt1_medium.jpg");
		 productModel.setProduct("iPad Pro");
		 productModel.setShortDescription("Anything you can do, you can do better.");
		 productModel.setLongDescription("No matter the task, the new iPad Pro is up to it — and then some. It offers far more power than most PC laptops, yet is delightfully simple to use. The redesigned Retina display is as stunning to look at as it is to touch.");
		 productModel.setStock(10);
				 
		 productService.create(productModel);   
		 		 
		 
		 productModel = new Product();
		 
		 productModel.setImage("https://images.apple.com/v/ipad/home/ac/images/home/pencil_small.jpg");
		 productModel.setProduct("Apple Pencil for iPad Pro");
		 productModel.setShortDescription("It’s only a magic wand.");
		 productModel.setLongDescription("The versatile Apple Pencil is the best tool to reach for when you need pixel‑perfect precision. Use it to jot down notes, draft a schematic, or paint a watercolor. ");
		 productModel.setStock(15);
				 
		 productService.create(productModel);   
		 
		 
		 productModel = new Product();
		 
		 productModel.setImage("https://images.apple.com/v/ipad/home/ac/images/home/smart_keyboard_small.jpg");
		 productModel.setProduct("Smart Keyboard for iPad Pro");
		 productModel.setShortDescription("Full-size keyboard. Full-screen protection.");
		 productModel.setLongDescription("The Smart Keyboard — available for both the 10.5‑inch and 12.9‑inch iPad Pro — provides a full-size keyboard to get your thoughts down and a durable cover for everyday protection.");
		 productModel.setStock(20);
				 
		 productService.create(productModel); 
		 
   	
   }    

    public List<Product> getAllProducts() {
    	
    	logger.debug("getAllProducts");
    	
    	List<Product> response = new ArrayList<Product>();
    	
        try {
        	
        	response = productService.getAllProducts();
        	
        	logger.debug("getAllProducts - success");
        
        } catch (Exception e) {
        	
        	logger.debug("getAllProducts - fail: " + e.getMessage() + e.getStackTrace());

        }
        
        return response;
    }

	public Product getProduct(@PathVariable("id") Long id) {
		
		logger.debug("getProduct");
		
		Product response = new Product();
		
		//try
			response = productService.getProductById(id);
			
			return response;
		
	}
	

	public void buyProduct(@PathVariable("id") Long id) {
		
		logger.debug("buyProduct");
		
		Product product = productService.getProductById(id); 
		
		product.setStock(product.getStock()-1);
		
		productService.update(product);
	}
	
	
	public void refillProduct(@PathVariable("id") Long id) {
		
		logger.debug("buyProduct");
		
		Product product = productService.getProductById(id); 
		
		product.setStock(product.getStock()+1);
		
		productService.update(product);
	}	

}
