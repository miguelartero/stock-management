package com.stock.controller.home;

import java.util.List;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stock.model.home.Product;

@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping(value = "/stock")
@EnableAutoConfiguration
public interface ProductController {
	
	@RequestMapping(value = "/createSomeInitialProducts", method = RequestMethod.POST)
    void createSomeInitialProducts(); 

	@RequestMapping(value = "/getAllProducts", method = RequestMethod.GET)
	List<Product>  getAllProducts();
	
	@RequestMapping(value = "/getProduct/{id}", method = RequestMethod.GET)
	Product getProduct (@PathVariable("id") Long id);
	
	@RequestMapping(value = "/buyProduct/{id}", method = RequestMethod.POST)
	void buyProduct (@PathVariable("id") Long id);
	
	@RequestMapping(value = "/refillProduct/{id}", method = RequestMethod.POST)
	void refillProduct (@PathVariable("id") Long id);	
	

}
