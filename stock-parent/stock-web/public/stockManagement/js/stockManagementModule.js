(function(){

	var app = angular.module('store', [ ]);
	
	app.directive('productNavbar', function() {
		return {
			restrict: 'E',
			templateUrl: 'stockManagement/html/product-navbar.html',
			controller: ['$scope', '$http', function($scope, $http) {
				
				$scope.showProducts = function() {
					$scope.showProduct = true;
				}
				
				$scope.showStock = function() {
					$scope.showProduct = false;
					
					$scope.stock = [ ];
					
					$http.get('/stock/getAllProducts')				
						 .then(function (response) {
							 $scope.stock = response.data;
					})					
				}	
			}],
			controllerAs: 'modal'
				
		}
	});		
	
	app.directive('productStore', function() {
		return {
			restrict: 'E',
			templateUrl: 'stockManagement/html/product-store.html',
			controller: ['$scope', '$http', function($scope, $http) {
				
				$scope.showProduct = true;
				
				var store = this;
				store.products = [ ];
				store.product = [ ];
				
				$http.get('/stock/getAllProducts')				
					 .then(function (response) {
						 store.products = response.data;
				})
				
				$scope.createSomeProduct = function() {
					$http.post('/stock/createSomeInitialProducts')
					.then(function (response) {
						
						$http.get('/stock/getAllProducts')				
						 .then(function (response) {
							 store.products = response.data;
						})
						
					})
				};
				
				$scope.getProduct = function(id) {
					
					$http.get('/stock/getProduct/'+id)				
					 .then(function (response) {
						 $scope.product = response.data;
					})
				}				
					
			}],
			controllerAs: 'store'
			
			
		}
	});
	
	app.directive('productStock', function() {
		return {
			restrict: 'E',
			templateUrl: 'stockManagement/html/product-stock.html',
			controller: ['$scope', '$http', function($scope, $http) {
				
				$scope.showProduct = false;
				
				$scope.refill = function(id) {
					
					$http.post('/stock/refillProduct/'+id)				
					 .then(function (response) {
							$http.get('/stock/getAllProducts')				
							 .then(function (response) {
								 $scope.stock = response.data;
							})	
					})
					
				}
	
			}],
			controllerAs: 'modal'
				
		}
	});
	
	app.directive('productModal', function() {
		return {
			restrict: 'E',
			templateUrl: 'stockManagement/html/product-modal.html',
			controller: ['$scope', '$http', function($scope, $http) {
				
				$scope.buyProduct = function(id) {
					
					$http.post('/stock/buyProduct/'+id)				
					 .then(function (response) {
						 //
					})
				}	
			}],
			controllerAs: 'modal'
				
		}
	});

})();