package com.stock.services.home;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stock.dao.home.ProductRepository;
import com.stock.model.home.Product;

@Service
@Transactional
public class ProductServiceImp implements ProductService {

@Autowired
 private ProductRepository productRepository;

 public void create(Product productModel) {
	 productRepository.create(productModel);
 }
 
 public List<Product> getAllProducts() {
	 return productRepository.getAllProducts();
 }
 
 public Product getProductById(Long id) {
	 return productRepository.getProductById(id);
 }
 
 public void update(Product product) {
	 productRepository.update(product);
 }
 
}
