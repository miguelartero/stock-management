package com.stock.services.home;

import java.util.List;

import com.stock.model.home.Product;

public interface ProductService {

 public void create(Product productModel);
 
 public List<Product> getAllProducts();
 
 public Product getProductById(Long id);
 
 public void update(Product product);
 
}