#Stock Management

This is the stock management application. 
It has been used: 
Spring-boot with Hibernate. 
H2 embedded database. 
Spring Test (Mock)
AngularJs 1.4.6 and a template of Bootstrap (with Apple products). 

#How it works: 
On the top there is two link which show either product list or stock list

#Product List
The stock management show a product list with a Buy buttom. Click on it to see a modal window to confirm buy it. It will reduce the stock.

#Stock list 
The Stock list will show a simple list. click on the name to incresae the stock. 

#UnitTest
 Just one unitTest has been included, the one which controller. 

#Spring Boot Security
 The application just use 1 Table to manage the stock of a product. There is not other table for roles / user even relatioship.
 
#Spring Validation
 There is not much validation. All endPoint are called without any validation.	
  
## Compile
In the stock-parent module:

	mvn clean install

## Run
The project should be place on "C:/rindus/" folder. Other case, application.properties need to be update: 

spring.datasource.url = jdbc:h2:file:C:/rindus/springBootDB;DB_CLOSE_ON_EXIT=FALSE

In the stock-web module:

    mvn clean spring-boot:run

